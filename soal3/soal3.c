#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <pthread.h>
#include <limits.h>
#include <ctype.h>
#include <zip.h>



const char *Unknown = "Unknown";
const char *Hidden = "Hidden";
const char *jpg = "jpg";
const char *jpeg = "jpeg";
const char *png = "png";
const char *c = "c";
const char *js = "js";
const char *sh = "sh";
const char *fbx = "fbx";
const char *gns3project = "gns3project";
const char *zip = "zip";
const char *pdf = "pdf";
const char *txt = "txt";
const char *gz = "tar.gz";
const char *gif = "gif";
const char *hex = "hex";
const char *bin = "bin";

void dir()
{
    mkdir(Unknown, S_IRWXU);
    mkdir(Hidden, S_IRWXU);
    mkdir(jpg, S_IRWXU);
    mkdir(jpeg, S_IRWXU);
    mkdir(png, S_IRWXU);
    mkdir(c, S_IRWXU);
    mkdir(js, S_IRWXU);
    mkdir(sh, S_IRWXU);
    mkdir(fbx, S_IRWXU);
    mkdir(gns3project, S_IRWXU);
    mkdir(zip, S_IRWXU);
    mkdir(pdf, S_IRWXU);
    mkdir(txt, S_IRWXU);
    mkdir(gz, S_IRWXU);
    mkdir(gif, S_IRWXU);
    mkdir(hex, S_IRWXU);
    mkdir(bin, S_IRWXU);
    exit(EXIT_SUCCESS);
}

int main()
{
    //Buka file zip hartakarun
    int err = 0;
    zip *z = zip_open("hartakarun.zip", 0, &err);

    //Cari yang akan di unzip
    const char *name = "Citador.js";
    struct zip_stat st;
    zip_stat_init(&st);
    zip_stat(z, name, 0, &st);

    //Alokasi memori untuk yang di decompress
    char *contents = new char[st.size];

    //Extract file yang diinginkan
    zip_file *f = zip_fopen(z, "Citador.js", 0);
    zip_fread(f, contents, st.size);
    zip_fclose(f);

    //And close the archive
    zip_close(z);
    dir();
}
