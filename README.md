# soal-shift-sisop-modul-3-ITB03-2022

soal-shift-sisop-modul-3-ITB03-2022

# Readme Modul 3
# Soal Nomor 1

| Dimas Bagus Rachmadani | 5027201034 |
| --- | --- |


Pengerjaan

1. Melakukan download dan unzip file dengan menjadikan 2 folder yang berbeda yaitu music dan quote. Unzip juga dilakukan menggunakan thread.

Untuk melakukan download saya menggunakan fungsi download yang berisi link drive file yang akan didownload.

```c
void download_quote(){
    char dl_quote[1000]=
    {
        "https://drive.google.com/file/d/1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt/view?usp=sharing"
    };
    char quotezip[1000]=
    {
        "quote.zip"
    };

    pid_t child_id;
    child_id= fork();
    int status;
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    if (child_id == 0){
        execl("/bin/wget", "wget", "-q", "--no-check-certificate", dl_quote, "-O", quotezip, NULL);
    }else{
        ((wait(&status)) > 0);
    }
}

void download_music(){
    char dl_music[1000]=
    {
        "https://drive.google.com/file/d/1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1/view?usp=sharing"
    };
    char musizzip[1000]=
    {
        "music.zip"
    };

    pid_t child_id;
    child_id = fork();
    int status;
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    if (child_id == 0){
        execl("/bin/wget", "wget", "-q", "--no-check-certificate", dl_music, "-O", musizzip, NULL);
    }else{
        ((wait(&status)) > 0);
    }
}
```

Setelah didowload, file zip akan diunzip dan diletakkan pada folder yang berbeda, masing-masing folder direktori music dan quote. Untuk membuat file menggunakan fungsi makedir untuk membuat direktori menggunakan execl.

```c
void makedir_quote(){
    pid_t child_id;
    child_id = fork();
    int status;
    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        execl("/bin/mkdir", "mkdir", "-p", "/home/dimas/unzip/quote", NULL);
    }else{
        (wait(&status)) > 0;
    }
}

void makedir_music(){
    pid_t child_id;
    child_id = fork();
    int status;
    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        execl("/bin/mkdir", "mkdir", "-p", "/home/dimas/unzip/music", NULL);
    }else{
        (wait(&status)) > 0;
    }
}
```

Kemudian dilakukan unzip file music.zip dan quote.zip. Unzip diharuskan berjalan bersamaan menggunakan thread. Pengerjaan unzip menggunakan fungsi unzip yang berisi perintah execl serta pembuatan thread agar bisa dilakukan eksekusi secara bersamaan.

```c
void* unzipfile(void *arg){
    char *argv1[] = {"unzip", "quote.zip", "-d", "/home/dimas/unzip/quote", NULL};
    char *argv2[] = {"unzip", "music.zip", "-d", "/home/dimas/unzip/music", NULL};
    unsigned long i=0;
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid[0])){
        child = fork();
        if(child==0){
            execv("/bin/unzip", argv1);
        }
    }else if(pthread_equal(id,tid[1])){
        child = fork();
        if(child==0){
            execv("/bin/unzip", argv2);
        }
    }    
    return NULL;
}
```

Ketika semua dijalankan akan menghasilkan ouput seperti ini:

![Untitled](img/ss1.png)
![Untitled](img/ss2.png)
![Untitled](img/ss3.png)
![Untitled](img/ss4.png)


### Kendala

Terdapat kendala dalam pengerjaan soal yaitu kurang mengertinya terkait implementasi fungsi base64 pada C sehingga terdapat error dalam pengerjaan soal.

# Soal Nomor 2

Pengerjaan

| Naftali Salsabila Kanaya Putri | 5027201012 |
| --- | --- |

untuk soal `2a` <br/>
`Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client.`

Ketika client terhubung dengan server maka client akan diberikan 2 pilihan yaitu register atau login.

```c
 if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char id[20], pass[20], input_akun[100];
    
    printf("Register atau Login? ");
    scanf("%s", pilihan);
```
Tampilan yakni sebagai berikut <br/>
![Untitled](img/pilihan.JPG)

Ketika client input pilihan register, maka client diminta memasukkan id username dan password yang kemudian disimpan pada `users.txt` dengan format `username:password`

```c
    strcpy(input_akun, id);
    strcat(input_akun, ":");
    strcat(input_akun, pass);
    input_akun[strlen(input_akun)] = '\0';
    send(sock , input_akun , strlen(input_akun) , 0 );

    if(pilihan[0]=='r'){
       printf("Register berhasil\n");
    }
```

tampilan untuk register <br/>
![Untitled](img/register.JPG)

tampilan pada `users.txt` <br/>
![Untitled](img/users._txt.JPG)

Ketika client input pilihan untuk login, maka client diminta untuk input data id username dan password kemudian akan dicarikan datanya pada `users.txt` jika terdapat datanya maka akan mengeluarkan output `Login berhasil`, namun apabila data tidak ditemukan pada `users.txt` maka akan mengeluarkan output `Login gagal`

```c
 else if(pilihan[0]=='l'){
        valread = read( sock , login_status, 1024);
        if(login_status[0]=='b'){
            printf("Login berhasil\n");
        }
        else{
            printf("Login gagal!!\n");
            return 0;
        }   
```
tampilan login <br/>
![Untitled](img/login.JPG)


# Soal Nomor 3

Pengerjaan

| Ariel Daffansyah Aliski | 5027201058 |
| --- | --- |

Berikut adalah file header yang saya gunakan

```c
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <pthread.h>
#include <limits.h>
#include <ctype.h>
#include <zip.h>
```

Jawab : untuk pengerjaan extraction file ini, saya menggunakan library LibZip, yang guna dari library tersebut adalah memproses file zip, dengan cara mencari nama file zip yang bersangkutan, membuka file tersebut, melakukan read pada file yang kita inginkan, dan file tersebut akan terextract dari zipnya, lalu menutup zip tersebut. Proses ini memang memakan satu persatu file, tetapi saya gunakan karena sewaktu itu sempat tidak diperbolehkan menggunakan exec()

```c
//Buka file zip hartakarun
    int err = 0;
    zip *z = zip_open("hartakarun.zip", 0, &err);

    //Cari yang akan di unzip
    const char *name = "Citador.js";
    struct zip_stat st;
    zip_stat_init(&st);
    zip_stat(z, name, 0, &st);

    //Alokasi memori untuk yang di decompress
    char *contents = new char[st.size];

    //Extract file yang diinginkan
    zip_file *f = zip_fopen(z, "Citador.js", 0);
    zip_fread(f, contents, st.size);
    zip_fclose(f);

    //exit dari zip
    zip_close(z);
```

Lalu, untuk tiap tiap file, harus dibuat sebuah folder untuk meletakkan file yang akan diextract nantinya. Untuk itu saya menggunakan fungsi mkdir()

```c
const char *Unknown = "Unknown";
const char *Hidden = "Hidden";
const char *jpg = "jpg";
const char *jpeg = "jpeg";
const char *png = "png";
const char *c = "c";
const char *js = "js";
const char *sh = "sh";
const char *fbx = "fbx";
const char *gns3project = "gns3project";
const char *zip = "zip";
const char *pdf = "pdf";
const char *txt = "txt";
const char *gz = "tar.gz";
const char *gif = "gif";
const char *hex = "hex";
const char *bin = "bin";

void dir()
{
    mkdir(Unknown, S_IRWXU);
    mkdir(Hidden, S_IRWXU);
    mkdir(jpg, S_IRWXU);
    mkdir(jpeg, S_IRWXU);
    mkdir(png, S_IRWXU);
    mkdir(c, S_IRWXU);
    mkdir(js, S_IRWXU);
    mkdir(sh, S_IRWXU);
    mkdir(fbx, S_IRWXU);
    mkdir(gns3project, S_IRWXU);
    mkdir(zip, S_IRWXU);
    mkdir(pdf, S_IRWXU);
    mkdir(txt, S_IRWXU);
    mkdir(gz, S_IRWXU);
    mkdir(gif, S_IRWXU);
    mkdir(hex, S_IRWXU);
    mkdir(bin, S_IRWXU);
    exit(EXIT_SUCCESS);
}
```

Dengan hasil file sebagai berikut

![Untitled](img/Untitled.png)

Lalu, file zip harus bisa dikirimkan dalam sistem client server, agar client bisa mengirimkan file zip hartakarun kepada server, pada client yang mengirim dibuat seperti berikut

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#define PORT    5500
#define IP      "127.0.0.1"
#define MAXBUF  1024

int main() {
    struct sockaddr_in serv_addr;
    int     s;
    int         sourse_fd;
    char        buf[MAXBUF];
    int         file_name_len, read_len;
    /* socket() */
    s = socket(AF_INET, SOCK_STREAM, 0);
    if(s == -1) {
        return 1;
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(IP);
    serv_addr.sin_port = htons(PORT);

    if(connect(s, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1) {
        perror("connect : ");
        printf("fail to connect.\n");
        close(s);
        return 1;
    }

    memset(buf, 0x00, MAXBUF);
    printf("write file name to send to the server:  ");
    scanf("%s", buf);

    printf(" > %s\n", buf);
    file_name_len = strlen(buf);

    send(s, buf, file_name_len, 0);
    sourse_fd = open(buf, O_RDONLY);
    if(!sourse_fd) {
        perror("Error : ");
        return 1;
    }

    while(1) {
        memset(buf, 0x00, MAXBUF);
        read_len = read(sourse_fd, buf, MAXBUF);
        send(s, buf, read_len, 0);
        if(read_len == 0) {
            break;
        }

    }

    return 0;
}
```

Sementara, untuk server yang menerimanya adalah sebagai berikut

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>     // read, write
#include <arpa/inet.h>
#include <sys/types.h>  // socket, bind, accept, open
#include <sys/socket.h> // socket, bind, listen, accept
#include <sys/stat.h>   // open
#include <fcntl.h>      // open
#include <errno.h>

#define PORT    5500
#define MAXBUF  1024

int main() {
    int server_sockfd;
    int client_sockfd;
    int des_fd; // file num
    struct sockaddr_in serveraddr, clientaddr;
    int client_len, read_len, file_read_len;    // length
    char buf[MAXBUF];

    int check_bind;
    client_len = sizeof(clientaddr);

    /* socket() */
    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(server_sockfd == -1) {
        perror("socket error : ");
        exit(0);
    }

    /* bind() */
    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family       = AF_INET;
    serveraddr.sin_addr.s_addr  = htonl(INADDR_ANY);
    serveraddr.sin_port         = htons(PORT);

    if(bind(server_sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) > 0) {
        perror("bind error : ");
        exit(0);
    }

    /* listen */
    if(listen(server_sockfd, 5) != 0) {
        perror("listen error : ");
    }

    while(1) {
        char file_name[MAXBUF]; // local val
        memset(buf, 0x00, MAXBUF);

        /* accept() */
        client_sockfd = accept(server_sockfd, (struct sockaddr *)&clientaddr, &client_len);
        printf("New Client Connect : %s\n", inet_ntoa(clientaddr.sin_addr));

        /* file name */
        read_len = read(client_sockfd, buf, MAXBUF);
        if(read_len > 0) {
            strcpy(file_name, buf);
            printf("%s > %s\n", inet_ntoa(clientaddr.sin_addr), file_name);
        } else {
            close(client_sockfd);
            break;
        }

        /* create file */

        des_fd = open(file_name, O_WRONLY | O_CREAT | O_EXCL, 0700);
        if(!des_fd) {
            perror("file open error : ");
            break;
        }   
        /* file save */
        while(1) {
            memset(buf, 0x00, MAXBUF);
            file_read_len = read(client_sockfd, buf, MAXBUF);
            write(des_fd, buf, file_read_len);
            if(file_read_len == EOF | file_read_len == 0) {
                printf("finish file\n");
                break;
            }

        }

        close(client_sockfd);
        close(des_fd);
    }
    close(server_sockfd);
    return 0;
}
```

Saat dijalankan akan seperti berikut

![Untitled](img/gambar1.png)

Sehingga proses client-server dapat bekerja dengan sesuai

## Revisi Nomor 3

- Kesalahan penamaan folder

Saya melakukan kesalahan dimana folder bernama modul3, padahal seharusnya shift3

![Untitled](img/gambar2.png)

- Pengkategorian file per folder dan dijalankan dalam proses thread

pada sebelumnya, saya belum bisa menkategorikan file yang sudah diextract menjadi 1 folder sesuai ekstensinya. Untuk melakukan tersebut dapat dilakukan sebagai berikut

```c
pthread_t tid[500];
char keepFile[2000][2000];
const char *name[] = {"jpg","jpeg", "png", "tar.gz", "c", "js", "zip", "gns3project","bin", "hex", "txt", "fbx", "gif" };
int makeDir(void) {
    
    for (int i = 0; i < 14; ++i) {
       mkdir(name[i], S_IRWXU);
    }
}

int isRegular(const char *path){
  struct stat path_stat;
  stat(path, &path_stat);
  return S_ISREG(path_stat.st_mode);
}

void *pindahFile(void *arg){
  char str[999];
  char buffer[999];
  char buffer2[999];
  char buffer3[999];
  char buffer4[999];
  char path[1000];
  char tempDest[1000];
  char cwd[1000];
  char fileName[1000];
  char fileExt[1000];
  char fileExt2[1000];

  getcwd(cwd, sizeof(cwd));
  strcpy(path, (char*) arg);
  int isFile = isRegular(path);

  if(access(path, F_OK) == -1){
      // printf("File %s: Sad, gagal:(\n", tempCurr);
      // pthread_exit(0);
      return (void *) 0;
  }

  if(!isFile){
    // printf("File %s: Sad, gagal:(\n", tempCurr);
    return (void *) 0;
    // pthread_exit(0);
  }

  strcpy(buffer4, path);

  char *fileExt3;
  char dot1 = '.';
  fileExt3 = strchr(buffer4, dot1);
  // printf("%s", fileExt3);

  strcpy(buffer, path);
  char *token=strtok(buffer, "/");
  while(token != NULL){
      sprintf(fileName, "%s", token);
      token = strtok(NULL, "/");
  }

  strcpy(buffer, path);
  strcpy(buffer2, fileName);
  strcpy(buffer3, fileName);
  // strcpy(buffer4, fileName);

  int count = 0;

  char *token2=strtok(buffer2, ".");
  // printf("%s", token2);
  sprintf(fileExt2, "%s", token2);

  
  while(token2 != NULL){
      count++;
      // printf("%d", count);
    //   printf("%s\n", token2);
      sprintf(fileExt, "%s", token2);
    //   printf("%s", fileExt);
      token2=strtok(NULL, ".");
    }
    // printf("%s", fileExt);
  char dot = '.';
  char first = buffer3[0];
//   printf("%s", fileExt2);
    // printf("%c", buffer3[0]);
  if(dot == first){
    strcpy(fileExt, "Hidden");
  }

  else if(count >= 3){
    strcpy(fileExt, fileExt3+1);
  }

  else if (count <=1 ){
    strcpy(fileExt, "Unknown");
  }

  for (int i = 0; i < sizeof(fileExt); i++){
      fileExt[i] = tolower(fileExt[i]);
  }

  strcpy(buffer, (char*) arg);
  mkdir(fileExt, 0777);

  strcat(cwd, "/");
  strcat(cwd,fileExt);
  strcat(cwd, "/");
  strcat(cwd, fileName);
  strcpy(tempDest, cwd);

    
  rename(buffer, tempDest);
   
  return (void *) 1;
  

}

void listFilesRecursively(char *basePath, int *iter)
{
    // int iter = 0;
  char path[1000];
  struct dirent *dp;
  DIR *dir = opendir(basePath);

  if (!dir){
      
      return;

  }

  while ((dp = readdir(dir)) != NULL)
  {
    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
      {

        strcpy(path, basePath);
        strcat(path, "/");
        strcat(path, dp->d_name);

        if(isRegular(path)){
          strcpy(keepFile[*iter], path);
          *iter = *iter + 1;
        }
        else{
          listFilesRecursively(path, iter);
        }

      }
  }
    closedir(dir);
}

int main(int argc, char *argv[]) {

  void *status;
  int flag;
  char basePath[1000];
  int iter = 0;
      char cwd[1000];
      getcwd(cwd, sizeof(cwd));
      strcpy(basePath, cwd);
      listFilesRecursively(basePath, &iter);

      pthread_t tid[iter];
      flag = 0;

      for(int i = 0; i<iter; i++){
          pthread_create(&tid[i], NULL, pindahFile, (void*)keepFile[i]);
          pthread_join(tid[i], &status);

      }
      exit(0);

}
```

Sekaligus untuk bagian threading, dilakukan seperti pada fungsi main yang tertulis

```c
pthread_t tid[iter];
      flag = 0;

      for(int i = 0; i<iter; i++){
          pthread_create(&tid[i], NULL, pindahFile, (void*)keepFile[i]);
          pthread_join(tid[i], &status);

```
