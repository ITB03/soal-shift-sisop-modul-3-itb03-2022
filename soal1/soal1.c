#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <pthread.h>
#include <wait.h>
#include <string.h>
#include <stdint.h>

pthread_t tid[4];
pid_t child;

void download_quote(){
    char dl_quote[1000]=
    {
        "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download"
    };
    char quotezip[1000]=
    {
        "quote.zip"
    };

    pid_t child_id;
    child_id= fork();
    int status;
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    if (child_id == 0){
        execl("/bin/wget", "wget", "-q", "--no-check-certificate", dl_quote, "-O", quotezip, NULL);
    }else{
        ((wait(&status)) > 0);
    }
}

void download_music(){
    char dl_music[1000]=
    {
        "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"
    };
    char musizzip[1000]=
    {
        "music.zip"
    };

    pid_t child_id;
    child_id = fork();
    int status;
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    if (child_id == 0){
        execl("/bin/wget", "wget", "-q", "--no-check-certificate", dl_music, "-O", musizzip, NULL);
    }else{
        ((wait(&status)) > 0);
    }
}

void* unzipfile(void *arg){
    char *argv1[] = {"unzip", "quote.zip", "-d", "/home/dimas/unzip/quote", NULL};
    char *argv2[] = {"unzip", "music.zip", "-d", "/home/dimas/unzip/music", NULL};
    unsigned long i=0;
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid[0])){
        child = fork();
        if(child==0){
            execv("/bin/unzip", argv1);
        }
    }else if(pthread_equal(id,tid[1])){
        child = fork();
        if(child==0){
            execv("/bin/unzip", argv2);
        }
    }    
    return NULL;
}

void makedir_quote(){
    pid_t child_id;
    child_id = fork();
    int status;
    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        execl("/bin/mkdir", "mkdir", "-p", "/home/dimas/unzip/quote", NULL);
    }else{
        (wait(&status)) > 0;
    }
}

void makedir_music(){
    pid_t child_id;
    child_id = fork();
    int status;
    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        execl("/bin/mkdir", "mkdir", "-p", "/home/dimas/unzip/music", NULL);
    }else{
        (wait(&status)) > 0;
    }
}
/*
void build_decode_table(){
    decoding_table = malloc(256);
}

void base64_cleanup(){
    free(decoding_table);
}

unsigned char *base64_decode(const char *data, size_t input_length, size_t *output_length){
    if(decoding_table == NULL) build_decode_table();

    if(input_length %4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if(data[input_length - 1] == '=') (*output_length)--;
    if(data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if(decoded_data == NULL) return NULL;
}
*/
int main(void){
    download_quote();
    download_music();
    makedir_quote();
    makedir_music();
    int i=0;
    int err;
    while(i<3){
        err = pthread_create(&(tid[i]),NULL,&unzipfile,NULL);
        if(err!=0){
            printf("\n can't create thread : [%s]",strerror(err));
        }else{
            printf("\n create thread success\n");
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    exit(0);
    return 0;
}